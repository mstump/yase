/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITE_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITE_NODE_H

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <vector>

namespace yase {

/// @brief The abstract composite node
///
/// Composites contain child behaviors, which they call with certain strategies.
class ABT_DLL_EXPORT CompositeNode : public BehaviorNode {
 public:
  /// @brief CompositeNode constructor - it is mandatory to name the node
  explicit CompositeNode(const std::string& name);

  /// @brief Ctor with extension
  CompositeNode(const std::string& name, Extension::UPtr extension_ptr);

  virtual ~CompositeNode() override = default;

  using Ptr = std::shared_ptr<CompositeNode>;

  /// @brief Add an additional child at end
  void addChild(BehaviorNode::Ptr child);

  size_t childrenCount() const;

  /// @brief Get certain child reference by index
  const BehaviorNode& child(size_t index) const;

  /// @brief Get certain child reference by index
  BehaviorNode& child(size_t index);


  /// @brief Lookup and register data from blackboard and call children
  void distributeData() final;

  // Terminates all other children
  void onTerminate() final;

 protected:
  BehaviorNode& checkedIndex(size_t index) const;

  void addCheck(const BehaviorNode::Ptr& child) const;

  using NodeVector = std::vector<BehaviorNode::Ptr>;
  // The stored children nodes
  NodeVector m_children_nodes{};

  friend class CompositeManipulator;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITE_NODE_H
