/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_BEHAVIOR_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_BEHAVIOR_NODE_H

#include "agnostic_behavior_tree/scoped_blackboard.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <string>

namespace yase {

// Execution states of a node
enum class NodeStatus { kIdle = 0, kRunning, kSuccess, kFailure };

// Convert the status into an output string
ABT_DLL_EXPORT std::string toStr(NodeStatus status, bool use_ansi_escape_code = false);

// Simulator specific Extensions can be inherited and composed to behavior nodes
class ABT_DLL_EXPORT Extension {
 public:
  Extension() = default;
  virtual ~Extension() = default;

  using UPtr = std::unique_ptr<Extension>;
};

// Abstract base class for Behavior Tree Nodes.
class ABT_DLL_EXPORT BehaviorNode {
 public:
  virtual ~BehaviorNode() { m_blackboard = nullptr; };
  BehaviorNode(const BehaviorNode& copy_from) = delete;
  BehaviorNode& operator=(const BehaviorNode& copy_from) = delete;
  BehaviorNode(BehaviorNode&&) = delete;
  BehaviorNode& operator=(BehaviorNode&&) = delete;

  using Ptr = std::shared_ptr<BehaviorNode>;

  // Method to invoke tick from the outside while ensuring the consistency of the node status and tick cycle tracking
  NodeStatus executeTick();

  // Called once right before behavior is ticked the first time.
  // THE CALL MUST RESET THE BEHAVIOR with all its necessary values!
  virtual void onInit() = 0;

  // Called after behavior is called the last time
  virtual void onTerminate();

  // Get node status
  NodeStatus status() const;

  // Get detailed execution info
  std::string executionInfo() const;

  // Get tree node name
  const std::string& name() const;

  // Get Blackboard to examine symbols
  Blackboard::Ptr blackboardBase() const;

  /// \brief Distribute data among subtree
  virtual void distributeData();

  // Get current tick cycle of node
  unsigned int tickCycle() const;

  // Checks if node was ticked in current tick cycle
  bool tickedInCurrentCycle() const;

  // Check if it is safe to access the extension
  inline bool extensionExists() const { return (m_extension != nullptr); }

  // Get extension reference (will throw if null)
  Extension& extension() { return extensionChecked(); }

  // Get extension reference (will throw if null)
  const Extension& extension() const;

  // Get extension reference (will throw if null)
  template <class T>
  T& extension() {
    return dynamic_cast<T&>(extensionChecked());
  }

  // Get extension reference (will throw if null)
  template <class T>
  const T& extension() const {
    return dynamic_cast<const T&>(extensionChecked());
  }

  /// \brief Check if node is attached to another node (part of tree).
  /// This could also indicate a root node.
  bool hasParent() const;

 protected:
  // Method which defines behavior logic of node
  virtual NodeStatus tick() = 0;

  /// \brief Lookup and register data from blackboard
  ///
  /// Called by distributeData() - can be called multiple times
  virtual void lookupAndRegisterData(Blackboard& /*blackboard*/);

  /// \brief Set detailed execution info
  void executionInfo(const std::string& new_execution_info);

 private:
  /// \brief BehaviorNode constructor with simulator specific extension
  ///
  /// Ctor is private and only accessible to friend classes: Action, Decorator and Composites
  BehaviorNode(const std::string& name, Extension::UPtr extension_ptr = nullptr);

  Extension& extensionChecked() const;

  /// \brief Set parent link
  void setParent(BehaviorNode& node);

  /// \brief Node name
  const std::string m_name;

  // Additional information about the execution process
  std::string m_execution_info{};

  // Node execution state
  NodeStatus m_status{NodeStatus::kIdle};

  // Tracks the last tick cycle
  unsigned int m_tick_cycle{0};

  // Pointer to parent node (if existent)
  BehaviorNode* m_parent_node{nullptr};

  // Local blackboard
  BlackboardOwner::Ptr m_blackboard{nullptr};

  // Simulator specific functionality extension
  const Extension::UPtr m_extension{nullptr};

  // Friend classes, which need full access to blackboard
  friend class CompositeNode;
  friend class DecoratorNode;
  friend class ActionNode;
  friend class DataProxyNode;
  friend class BehaviorManipulator;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_BEHAVIOR_NODE_H
