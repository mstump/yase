/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_CHOOSER_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_CHOOSER_H

#include "agnostic_behavior_tree/composite_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

/// @brief Chooses the first non failing child and executes it until it succeeds.
///
/// The chooser node behaves similar to the selector node, but instead of selecting the sub-branch each
/// time the node is called anew, it selects only once.
class ChooserNode : public CompositeNode {
 public:
  explicit ChooserNode(const std::string& name = "Unnamed");

  explicit ChooserNode(Extension::UPtr extension_ptr);

  ChooserNode(const std::string& name, Extension::UPtr extension_ptr);

  virtual ~ChooserNode() override = default;

  void onInit() override;

  /// \brief Get the selected child
  /// \return nullptr if no child was selected so far or if selection failed
  std::unique_ptr<size_t> getSelectedChildIndex() const;

 private:
  NodeStatus tick() final;

 private:
  // Index of last running child - If null, then no child was active in step before
  std::unique_ptr<size_t> m_selected_child_index{nullptr};
};
}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SELECTOR_H
