/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_PARALLEL_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_PARALLEL_H

#include "agnostic_behavior_tree/composite_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <set>

namespace yase {

/// @brief The parallel composite
///
/// The parallel node ticks all children until all of them return kSuccess.
/// Once a child returns a kFailure. the parallel node is also set to kFailure.
class ParallelNode : public CompositeNode {
 public:
  /// @brief Policy to stop parallel execution
  enum class StopPolicy {
    /// kAllChildrenSucceed:  All nodes are executed until ALL succeed
    kAllChildrenSucceed,
    /// kFirstChildSucceeds:  All nodes are executed until FIRST node (in order) succeeds
    kFirstChildSucceeds,
    /// kAnyChildSucceeds:    All nodes are executed until ANY node succeeds
    kAnyChildSucceeds
  };

  /// @brief Converts stop policy to string
  std::string toStr(const StopPolicy& stop_policy);

  /// @brief Parallel node config
  struct Config {
    Config(){};
    StopPolicy stop_policy{StopPolicy::kAllChildrenSucceed};
  };

  ParallelNode(const std::string& name = "Unnamed", const Config config = Config{});

  explicit ParallelNode(Extension::UPtr extension_ptr, const Config config = Config{});

  ParallelNode(const std::string& name, Extension::UPtr extension_ptr, const Config config = Config{});

  virtual ~ParallelNode() = default;

  void onInit() override;

  /// @brief Returns all finished children
  std::set<size_t> getFinishedChildren() { return m_finished_children; };

 private:
  NodeStatus tick() final;

  /// @brief List of already finished children
  std::set<size_t> m_finished_children{};

  /// @brief Config of parallel node
  const Config m_config{};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_PARALLEL_H
