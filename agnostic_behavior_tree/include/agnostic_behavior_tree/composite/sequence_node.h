/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SEQUENCE_H
#define AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SEQUENCE_H

#include "agnostic_behavior_tree/composite_node.h"

namespace yase {
// The Sequence node
//
// The Sequence executes all children once in sequence.
// If a child behavior returns kRunning, it is executed in the next step again.
// Once it returns kSuccess, the next child is executed immediately in the same step.
// No child behavior is allowed to fail, if this happens, the whole Sequence fails
class SequenceNode : public CompositeNode {
 public:
  explicit SequenceNode(const std::string& name = "Unnamed");

  explicit SequenceNode(Extension::UPtr extension_ptr);

  SequenceNode(const std::string& name, Extension::UPtr extension_ptr);

  virtual ~SequenceNode() override = default;

  void onInit() override;

  /// \brief Get the last running child
  size_t getCurrentChild() const;

 private:
  NodeStatus tick() final;

  size_t m_current_child{0};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_COMPOSITION_SEQUENCE_H
