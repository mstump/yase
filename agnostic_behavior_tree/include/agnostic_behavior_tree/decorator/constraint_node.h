/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_CONSTRAINT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_CONSTRAINT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/condition.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

// Evaluation phase of a constraint:
//
// kPre: Constraint is checked once, before sub behavior is executed
// kRuntime: Constraint is checked always before sub behavior is executed
// kPost: Constraint is checked after sub behavior returned Success
enum class EvaluationPhase { kPre, kRuntime, kPost };

std::string ABT_DLL_EXPORT toStr(const EvaluationPhase& phase);

using TimedConstraint = std::pair<EvaluationPhase, Condition::UPtr>;

// This decorator checks constraints for a subtree.
class ABT_DLL_EXPORT ConstraintNode : public DecoratorNode {
 public:
  explicit ConstraintNode(TimedConstraint condition, Extension::UPtr extension_ptr = nullptr);

  virtual ~ConstraintNode() override = default;

 private:
  // Override to prevent too soon initialisation of child -> should only initialised after pre constraint is fulfilled
  void onInit() final;

  // Evaluate the necessary constraints before executing the decorated child node
  NodeStatus tick() final;

  // Lookup data for all constraints
  void lookupAndRegisterData(Blackboard& blackboard) final;

  // Convenience method
  std::string createErrorMsg(const std::string& constraint_time, const std::string& constraint_info);

  // Managed constraints
  Condition::UPtr m_init_constraint{nullptr};
  Condition::UPtr m_pre_constraint{nullptr};
  Condition::UPtr m_runtime_constraint{nullptr};
  Condition::UPtr m_post_constraint{nullptr};

  bool m_first_call{true};
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_CONSTRAINT_NODE_H
