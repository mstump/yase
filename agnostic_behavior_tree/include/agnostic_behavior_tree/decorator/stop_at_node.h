/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_DECORATOR_STOP_AT_NODE_H
#define AGNOSTIC_BEHAVIOR_TREE_DECORATOR_STOP_AT_NODE_H

#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/condition.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

// Stops child behavior execution at positive condition evaluation
//
// - If child behavior finishes earlier, it will also finish
class ABT_DLL_EXPORT StopAtNode : public DecoratorNode {
 public:
  explicit StopAtNode(Condition::UPtr condition, Extension::UPtr extension_ptr = nullptr);

  virtual ~StopAtNode() override = default;

 private:
  // Executes child behavior until condition is evaluated to true
  NodeStatus tick() final;

  // LookUp data for condition
  void lookupAndRegisterData(Blackboard& blackboard) final;

  // onInit() is called is called directly before first evaluation - resets the behavior
  void onInit() final;

  // The condition to evaluate
  Condition::UPtr m_condition;
};

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_DECORATOR_STOP_AT_NODE_H
