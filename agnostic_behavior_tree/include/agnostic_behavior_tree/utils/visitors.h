/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_VISITORS_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_VISITORS_H

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

#include <functional>

namespace yase {

/// @brief Allows to execute a generic Functor on every node
///
/// @param root_node is the node to start the print
/// @param visitor is the visitor
ABT_DLL_EXPORT void applyVisitor(const BehaviorNode& root_node,
                                 const std::function<void(const BehaviorNode&)>& visitor);

/// @brief  Prints a tree starting from the given node
///
/// @param root_node is the node to start the print
/// @param print_visitor is the visitor to print the node
ABT_DLL_EXPORT void applyPrintVisitor(
    const BehaviorNode& root_node,
    const std::function<void(const BehaviorNode&, const std::string& start_prefix, const std::string& follow_prefix)>&
        print_visitor);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_VISITORS_H
