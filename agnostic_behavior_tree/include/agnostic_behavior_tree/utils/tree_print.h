/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_PRINT_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_PRINT_H

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"
#include "agnostic_behavior_tree/utils/visitors.h"

#include <iostream>

namespace yase {

/// @brief Converts the node into an output string
///
/// @param[in] node is the node instance to be converted to a string
/// @param[in] ansi_escape_code allows to print in ansi color code
/// @param[in] print_ticks prints additionally the execution tick
ABT_DLL_EXPORT std::string nodeToStr(const BehaviorNode& node,
                                     const bool ansi_escape_code = true,
                                     const bool print_ticks = false);

/// @brief Prints a node with its subtree
///
/// @param[in] node is the node instance to start
/// @param[in] out_stream to print on
ABT_DLL_EXPORT void printTree(const BehaviorNode& node, std::ostream& out_stream = std::cout);

/// @brief Prints a node with its subtree with the registered blackboard data of each node
///
/// @param[in] node is the node instance to start
/// @param[in] out_stream to print on
ABT_DLL_EXPORT void printTreeWithRegisteredBlackboardData(const BehaviorNode& node,
                                                          std::ostream& out_stream = std::cout);

/// @brief Prints a node with its subtree with the state and execution info
///
/// @param[in] node is the node instance to start
/// @param[in] out_stream to print on
ABT_DLL_EXPORT void printTreeWithStates(const BehaviorNode& node, std::ostream& out_stream = std::cout);

/// @brief Prints a node with its subtree with failure traceback
///
/// Also captured failures are printed even they did not cause the failure of the root
///
/// @param[in] node is the node instance to start
/// @param[in] out_stream to print on
ABT_DLL_EXPORT void printTreeWithFailureTraces(const BehaviorNode& node, std::ostream& out_stream = std::cout);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_TREE_PRINT_H
