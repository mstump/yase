/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_MANIPULATORS_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_MANIPULATORS_H

#include "agnostic_behavior_tree/composite_node.h"
#include "agnostic_behavior_tree/decorator_node.h"
#include "agnostic_behavior_tree/utils/dll_export_helper.h"

namespace yase {

/// @brief Behavior node manipulator. Enables the user to further manipulate a behavior node by clearing the parent link
class ABT_DLL_EXPORT BehaviorManipulator {
 public:
  explicit BehaviorManipulator(BehaviorNode& behavior);

  /// @brief Forward hasParent call to behavior
  bool hasParent() const;

  /// @brief Forward setParent call to behavior
  void setParent(BehaviorNode& node);

  /// @brief Forward setParent call to behavior
  void setParent(BehaviorNode::Ptr node);

  /// @brief Clear parent link
  /// Use this method to remove link of node from behavior tree
  void clearParent() const;

 private:
  BehaviorNode& m_behavior;
};

/// @brief Helper for direct manipulation calls
BehaviorManipulator manipulator(BehaviorNode& behavior);

/// @brief Helper for direct manipulation calls
BehaviorManipulator manipulator(BehaviorNode::Ptr behavior);

/// @brief Decorator node manipulator. Enables the user to further manipulate a decorator node by removing a child node
class ABT_DLL_EXPORT DecoratorManipulator : public BehaviorManipulator {
 public:
  explicit DecoratorManipulator(DecoratorNode& decorator);

  /// @brief Forward setChild call to decorator
  void setChild(BehaviorNode::Ptr child);

  /// @brief Forward hasChild call to decorator
  bool hasChild() const;

  /// @brief Detach child from decorator
  /// The child is detached from parent and blackboard. The internal child is set to nullptr.
  BehaviorNode::Ptr detachChild();

 private:
  DecoratorNode& m_decorator;
};

/// @brief Helper for direct manipulation calls
DecoratorManipulator manipulator(DecoratorNode& decorator);

/// @brief Helper for direct manipulation calls
DecoratorManipulator manipulator(DecoratorNode::Ptr decorator);

/// @brief Composite node manipulator. Enables the user to further manipulate a composite node by inserting, replacing
/// and removing a child node
class ABT_DLL_EXPORT CompositeManipulator : public BehaviorManipulator {
 public:
  explicit CompositeManipulator(CompositeNode& composite);

  /// @brief Forward addChild call to composite
  void addChild(BehaviorNode::Ptr child);

  /// @brief Check if child exist
  bool hasChild(size_t index) const;

  /// @brief Insert an additional child
  void insertChild(BehaviorNode::Ptr child, size_t index);

  /// @brief Replace existing child with new child and returns detached child
  BehaviorNode::Ptr replaceChild(BehaviorNode::Ptr child, size_t index);

  /// @brief Detach child from composite and return node
  BehaviorNode::Ptr detachChild(size_t index);

 private:
  CompositeNode& m_composite;
};

/// @brief Helper for direct manipulation calls
CompositeManipulator manipulator(CompositeNode& composite);

/// @brief Helper for direct manipulation calls
CompositeManipulator manipulator(CompositeNode::Ptr composite);

}  // namespace yase

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_MANIPULATORS_H
