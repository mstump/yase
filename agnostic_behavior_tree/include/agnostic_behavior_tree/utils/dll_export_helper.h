/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef AGNOSTIC_BEHAVIOR_TREE_UTILS_DLL_EXPORT_HELPER_H
#define AGNOSTIC_BEHAVIOR_TREE_UTILS_DLL_EXPORT_HELPER_H

#ifdef _MSC_VER

#  define ABT_DLL_EXPORT __declspec(dllexport)

#else

#  define ABT_DLL_EXPORT

#endif

#endif  // AGNOSTIC_BEHAVIOR_TREE_UTILS_DLL_EXPORT_HELPER_H
