# ##############################################################################
# Copyright (c) 2022 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

project(AgnosticBehaviorTree)

add_subdirectory(src)

if(Yase_BUILD_TESTS)
  add_subdirectory(tests)
endif()

if(Yase_BUILD_DOCS)
  add_subdirectory(doc)
endif()

if(Yase_INSTALL)
  install(DIRECTORY include/ TYPE INCLUDE)
endif()
