/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/composite/parallel_node.h"

#include <gtest/gtest.h>

namespace yase {

TEST(ParallelTest, failing_parallel) {
  // Parallel with three children. One is failing
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>();
  test_parallel_node->addChild(std::make_shared<AlwaysRunning>());
  test_parallel_node->addChild(std::make_shared<AlwaysFailure>());

  // helper variables
  NodeStatus status = NodeStatus::kRunning;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
  }

  EXPECT_EQ(status, NodeStatus::kFailure);
}

// Test if parallel node inits the children at start and terminates them as they finish
TEST(ParallelTest, test_onInit_and_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(2);

  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>();
  test_parallel_node->addChild(dummy_node_1);
  test_parallel_node->addChild(dummy_node_2);

  // Before the dummy_nodes should be uninitialized
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);

  // Init and tick of parallel (should init children as well)
  test_parallel_node->onInit();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), true);
  test_parallel_node->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), true);

  // Second tick
  test_parallel_node->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), true);

  // Third tick (first dummy is done and terminated)
  test_parallel_node->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);
}

// Test if parallel node terminates all children on sudden terminate
TEST(ParallelTest, test_sudden_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(2);

  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>();
  test_parallel_node->addChild(dummy_node_1);
  test_parallel_node->addChild(dummy_node_2);

  // Init and tick of parallel (should init children as well)
  test_parallel_node->onInit();
  test_parallel_node->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), true);

  // Sudden terminate
  test_parallel_node->onTerminate();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);
}

/// Test if parallel finishes once the all children succeed
TEST(ParallelTest, stop_policy_all_success) {
  // Parallel with three children - Longest one takes 3+1 ticks
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>();
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(2));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Should finish successful with 3+1 ticks
  EXPECT_EQ(status, NodeStatus::kSuccess);
  EXPECT_EQ(counter, 4);
}

/// Test if parallel ends with failure
TEST(ParallelTest, stop_policy_all_success_ends_with_failure) {
  // Parallel with three children
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>();
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1, NodeStatus::kFailure));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Should end with a failure after 1+1 ticks
  EXPECT_EQ(status, NodeStatus::kFailure);
  EXPECT_EQ(counter, 2);
}

/// Test if parallel stops once the first child succeeds
TEST(ParallelTest, stop_policy_first_success) {
  ParallelNode::Config config{};
  config.stop_policy = ParallelNode::StopPolicy::kFirstChildSucceeds;

  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>("", config);
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(2));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Parallel with three children - First one takes 2+1 ticks
  EXPECT_EQ(status, NodeStatus::kSuccess);
  EXPECT_EQ(counter, 3);
}

/// Test if parallel kFirstChildSucceeds ends with failure while there is another success
TEST(ParallelTest, stop_policy_first_success_ends_with_failure_1) {
  ParallelNode::Config config{};
  config.stop_policy = ParallelNode::StopPolicy::kFirstChildSucceeds;

  // Parallel with three children
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>("", config);
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1, NodeStatus::kFailure));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Should finish end with failure after 1+1 ticks
  EXPECT_EQ(status, NodeStatus::kFailure);
  EXPECT_EQ(counter, 2);
}

/// Test if parallel kFirstChildSucceeds ends with failure
TEST(ParallelTest, stop_policy_first_success_ends_with_failure_2) {
  ParallelNode::Config config{};
  config.stop_policy = ParallelNode::StopPolicy::kFirstChildSucceeds;

  // Parallel with three children
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>("", config);
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(2));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1, NodeStatus::kFailure));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Should finish end with failure after 1+1 ticks
  EXPECT_EQ(status, NodeStatus::kFailure);
  EXPECT_EQ(counter, 2);
}

/// Test if parallel stops once any child succeeds
TEST(ParallelTest, stop_policy_any_success) {
  ParallelNode::Config config{};
  config.stop_policy = ParallelNode::StopPolicy::kAnyChildSucceeds;

  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>("", config);
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(2));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Parallel with three children - First one takes 1+1 ticks
  EXPECT_EQ(status, NodeStatus::kSuccess);
  EXPECT_EQ(counter, 2);
}

/// Test if parallel kAnyChildSucceeds ends with failure while there is another success
TEST(ParallelTest, stop_policy_any_success_ends_with_failure_1) {
  ParallelNode::Config config{};
  config.stop_policy = ParallelNode::StopPolicy::kAnyChildSucceeds;

  // Parallel with three children
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>("", config);
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1, NodeStatus::kFailure));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Should finish end with failure after 1+1 ticks
  EXPECT_EQ(status, NodeStatus::kFailure);
  EXPECT_EQ(counter, 2);
}

/// Test if parallel kAnyChildSucceeds ends with failure
TEST(ParallelTest, stop_policy_any_success_ends_with_failure_2) {
  ParallelNode::Config config{};
  config.stop_policy = ParallelNode::StopPolicy::kAnyChildSucceeds;

  // Parallel with three children
  CompositeNode::Ptr test_parallel_node = std::make_shared<ParallelNode>("", config);
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(2));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(1, NodeStatus::kFailure));
  test_parallel_node->addChild(std::make_shared<AnalyseNode>(3));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;
  size_t counter = 0;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_parallel_node->executeTick();
    counter++;
  }

  // Should finish end with failure after 1+1 ticks
  EXPECT_EQ(status, NodeStatus::kFailure);
  EXPECT_EQ(counter, 2);
}

}  // namespace yase
