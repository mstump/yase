/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/composite/chooser_node.h"

#include <gtest/gtest.h>
#include <memory>

namespace yase {

TEST(ChooserTest, direct_successful_chooser) {
  // Second children is directly success
  CompositeNode::Ptr test_chooser = std::make_shared<ChooserNode>();
  test_chooser->addChild(std::make_shared<AlwaysFailure>());
  test_chooser->addChild(std::make_shared<AlwaysSuccess>());

  // Should directly finish with success
  EXPECT_EQ(test_chooser->executeTick(), NodeStatus::kSuccess);
}

TEST(ChooserTest, successful_chooser) {
  // Second children is successful after multiple ticks
  CompositeNode::Ptr test_chooser = std::make_shared<ChooserNode>();
  test_chooser->addChild(std::make_shared<AlwaysFailure>());
  test_chooser->addChild(std::make_shared<AnalyseNode>(2));

  // Helper variables
  NodeStatus status = NodeStatus::kRunning;

  // Execute until finished
  while (status == NodeStatus::kRunning) {
    status = test_chooser->executeTick();
  }

  // Should finish successfully
  EXPECT_EQ(status, NodeStatus::kSuccess);
  EXPECT_EQ(test_chooser->tickCycle(), 3);
}

TEST(ChooserTest, failing_chooser) {
  // None of the children succeeds/ can run
  CompositeNode::Ptr test_chooser = std::make_shared<ChooserNode>();
  test_chooser->addChild(std::make_shared<AlwaysFailure>());
  test_chooser->addChild(std::make_shared<AlwaysFailure>());

  // Helper variables
  NodeStatus status = test_chooser->executeTick();

  // Should fail
  EXPECT_EQ(status, NodeStatus::kFailure);
}

// Test if chooser inits the children at start and terminates them as they finish
TEST(ChooserTest, test_onInit_and_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AlwaysFailure>();
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(1, NodeStatus::kFailure);
  std::shared_ptr<AnalyseNode> dummy_node_3 = std::make_shared<AlwaysRunning>();

  CompositeNode::Ptr chooser = std::make_shared<ChooserNode>();
  chooser->addChild(dummy_node_1);
  chooser->addChild(dummy_node_2);
  chooser->addChild(dummy_node_3);

  // Before the dummy_nodes should be uninitialized
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->isInitialised(), false);

  // Init and tick of chooser - Second child runs
  chooser->onInit();
  chooser->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_1->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_2->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 0);

  EXPECT_EQ(dummy_node_3->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->onInitCalls(), 0);
  EXPECT_EQ(dummy_node_3->onTerminateCalls(), 0);

  // Second tick - Second child finishes
  chooser->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_1->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_2->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_3->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->onInitCalls(), 0);
  EXPECT_EQ(dummy_node_3->onTerminateCalls(), 0);

  // Third tick - chooser already finished, nothing should change
  chooser->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), false);
  EXPECT_EQ(dummy_node_1->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_2->isInitialised(), false);
  EXPECT_EQ(dummy_node_2->onInitCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 1);

  EXPECT_EQ(dummy_node_3->isInitialised(), false);
  EXPECT_EQ(dummy_node_3->onInitCalls(), 0);
  EXPECT_EQ(dummy_node_3->onTerminateCalls(), 0);
}

// Test if sequence node terminates all children on sudden terminate
TEST(ChooserTest, test_sudden_onTerminate) {
  // Setup
  std::shared_ptr<AnalyseNode> dummy_node_1 = std::make_shared<AnalyseNode>(1);
  std::shared_ptr<AnalyseNode> dummy_node_2 = std::make_shared<AnalyseNode>(2);

  CompositeNode::Ptr chooser = std::make_shared<ChooserNode>();
  chooser->addChild(dummy_node_1);
  chooser->addChild(dummy_node_2);

  // Init and tick of parallel (should init children as well)
  chooser->onInit();
  chooser->executeTick();
  EXPECT_EQ(dummy_node_1->isInitialised(), true);
  EXPECT_EQ(dummy_node_2->isInitialised(), false);

  // Sudden terminate - Children onTerminate should be called
  chooser->onTerminate();
  EXPECT_EQ(dummy_node_1->onTerminateCalls(), 1);
  EXPECT_EQ(dummy_node_2->onTerminateCalls(), 1);
}

}  // namespace yase
