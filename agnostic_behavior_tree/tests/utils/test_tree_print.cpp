/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/actions/functor_action_node.h"
#include "agnostic_behavior_tree/composite/parallel_node.h"
#include "agnostic_behavior_tree/decorator/data_declaration_node.h"
#include "agnostic_behavior_tree/decorator/data_proxy_node.h"
#include "agnostic_behavior_tree/utils/tree_print.h"

#include <gtest/gtest.h>
#include <memory>

namespace yase {

// Defines multiple test instances to test the functionality
struct TreePrintFixture : testing::Test {
  TreePrintFixture() {
    // Build up a complex tree to print

    // A parallel node to fork the tree into two subtrees (is the child of ego_param_declarer_decorator)
    CompositeNode::Ptr root = std::make_shared<ParallelNode>();
    root->addChild(std::make_shared<AlwaysSuccess>());

    // Top node: makes road everywhere downstream available
    DataDeclaration::UPtr ego_param_declarer = std::make_unique<EgoParamInserter>();
    DecoratorNode::Ptr ego_param_declarer_decorator =
        std::make_shared<DataDeclarationNode>("RoadDeclarer", std::move(ego_param_declarer));
    root->addChild(ego_param_declarer_decorator);
    root->addChild(std::make_shared<AlwaysRunning>());

    // A parallel node to fork the tree into two subtrees (is the child of ego_param_declarer_decorator)
    CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
    ego_param_declarer_decorator->setChild(parallel);

    // Child 1: wants to access the ego params --> legal, as declared upstream
    parallel->addChild(std::make_shared<TestNodeAccessEgoParams>());

    // Child 2: Adds argument proxy decorator between parallel and leaf child
    ProxySetting::UPtr arg_proxy = std::make_unique<ArgProxy>();
    DecoratorNode::Ptr arg_proxy_decorator = std::make_shared<DataProxyNode>("EgoParamDeclarer", std::move(arg_proxy));
    arg_proxy_decorator->setChild(std::make_shared<TestNodeAccessProxyArgs>());
    parallel->addChild(std::move(arg_proxy_decorator));
    parallel->addChild(std::make_shared<AlwaysSuccess>());
    parallel->addChild(std::make_shared<AlwaysRunning>());

    // The the collision will be observed in three ticks, which causes a failure downstream:
    ego_param_declarer_decorator->distributeData();

    test_tree = root;
  };

  virtual ~TreePrintFixture(){};

  // Test tree
  BehaviorNode::Ptr test_tree{nullptr};

  // ------------- TEST INSTANCES -----------------

  // Exemplary declarer to provide ego params
  class EgoParamInserter : public DataDeclaration {
   public:
    // Get shared data
    void lookupAndRegisterData(Blackboard& blackboard) final {
      blackboard.set<std::shared_ptr<unsigned int>>("ego_id", std::make_shared<unsigned int>(1));
      blackboard.set<std::shared_ptr<double>>("ego_target_speed", std::make_shared<double>(30.0));
    };
  };

  class ArgProxy : public ProxySetting {
   public:
    // GetSharedData
    Blackboard::ProxyTable createProxyTableAndDeclareData(Blackboard& blackboard) final {
      Blackboard::ProxyTable proxy;
      // Remap key to other value
      proxy.insert({"vehicle_id", "ego_id"});
      // If no remapping should be done, set some default instead:
      blackboard.set<std::shared_ptr<size_t>>("target_lane", std::make_shared<size_t>(707));
      return proxy;
    };
  };

  // Node which requires access to the ego params
  class TestNodeAccessEgoParams : public ActionNode {
   public:
    TestNodeAccessEgoParams() : ActionNode("TestNodeAccessEgoParams"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (*m_ego_id != 1 && *m_ego_target_speed != 30.0) {
        executionInfo("The ego params are not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbols
    void lookupAndRegisterData(Blackboard& blackboard) final {
      m_ego_id = blackboard.get<std::shared_ptr<unsigned int>>("ego_id");
      m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
    };

    std::shared_ptr<unsigned int> m_ego_id{nullptr};
    std::shared_ptr<double> m_ego_target_speed{nullptr};
  };

  // Node which requires access to all proxy arguments
  class TestNodeAccessProxyArgs : public ActionNode {
   public:
    TestNodeAccessProxyArgs() : ActionNode("TestNodeAccessProxyArgs"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (*m_vehicle_id != 1 && *m_target_lane != 707) {
        executionInfo("The proxy args are not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared symbols can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// look up required symbols
    void lookupAndRegisterData(Blackboard& blackboard) final {
      m_vehicle_id = blackboard.get<std::shared_ptr<unsigned int>>("vehicle_id");
      m_target_lane = blackboard.get<std::shared_ptr<size_t>>("target_lane");
    };

    std::shared_ptr<unsigned int> m_vehicle_id{nullptr};
    std::shared_ptr<size_t> m_target_lane{nullptr};
  };
};

TEST_F(TreePrintFixture, print_tree) { EXPECT_NO_THROW(printTree(*test_tree);); }

TEST_F(TreePrintFixture, print_tree_with_states) {
  EXPECT_NO_THROW(printTreeWithStates(*test_tree););
  test_tree->executeTick();
  EXPECT_NO_THROW(printTreeWithStates(*test_tree););
}

TEST_F(TreePrintFixture, print_tree_with_registered_data) {
  EXPECT_NO_THROW(printTreeWithRegisteredBlackboardData(*test_tree););
}

}  // namespace yase
