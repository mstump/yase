/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/actions/functor_action_node.h"
#include "agnostic_behavior_tree/composite/parallel_node.h"
#include "agnostic_behavior_tree/decorator/data_declaration_node.h"

#include <gtest/gtest.h>
#include <vector>

namespace yase {

// Defines multiple test instances to test the functionality
struct DataDeclarationNodeFixture : testing::Test {
  DataDeclarationNodeFixture(){};
  virtual ~DataDeclarationNodeFixture(){};

  // ------------- TEST INSTANCES -----------------
  struct RoadDummy {
    unsigned int laneId{0};
  };

  // Exemplary declarer
  class RoadInserter : public DataDeclaration {
   public:
    // Registers road data
    void lookupAndRegisterData(Blackboard& blackboard) final {
      RoadDummy road;
      road.laneId = 707;
      blackboard.set<std::shared_ptr<RoadDummy>>("road", std::make_shared<RoadDummy>(road));
    };
  };

  // Exemplary declarer
  class EgoParamInserter : public DataDeclaration {
   public:
    // Registers ego params
    void lookupAndRegisterData(Blackboard& blackboard) final {
      blackboard.set<unsigned int>("ego_id", 1);
      blackboard.set<std::shared_ptr<double>>("ego_target_speed", std::make_shared<double>(30.0));
    };
  };

  // Node which requires access to road
  class TestNodeAccessRoad : public ActionNode {
   public:
    TestNodeAccessRoad() : ActionNode("TestNodeAccessRoad"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (m_road->laneId != 707) {
        executionInfo("The road seems not be accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared data can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// Look up required data "vehicle_collisions"
    void lookupAndRegisterData(Blackboard& blackboard) final {
      m_road = blackboard.get<std::shared_ptr<RoadDummy>>("road");
    };

    std::shared_ptr<RoadDummy> m_road{};
  };

  // Node which requires access to road and ego params
  class TestNodeAccessAllParams : public ActionNode {
   public:
    TestNodeAccessAllParams() : ActionNode("TestNodeAccessAllParams"){};

    void onInit() override{};

   private:
    NodeStatus tick() final {
      if (m_road->laneId != 707) {
        executionInfo("The road seems not be accessible as expected!");
        return NodeStatus::kFailure;
      }
      if (m_ego_id != 1) {
        executionInfo("The ego_id is not accessible as expected!");
        return NodeStatus::kFailure;
      }
      if (*(m_ego_target_speed) != 30.0) {
        executionInfo("The ego_target_speed is not accessible as expected!");
        return NodeStatus::kFailure;
      }
      executionInfo("All shared data can be accesssed.");
      return NodeStatus::kSuccess;
    };

    /// Look up required data "vehicle_collisions"
    void lookupAndRegisterData(Blackboard& blackboard) final {
      m_road = blackboard.get<std::shared_ptr<RoadDummy>>("road");
      m_ego_id = blackboard.get<unsigned int>("ego_id");
      m_ego_target_speed = blackboard.get<std::shared_ptr<double>>("ego_target_speed");
    };

    std::shared_ptr<RoadDummy> m_road{};
    unsigned int m_ego_id{};
    std::shared_ptr<double> m_ego_target_speed{};
  };
};

// Legal tree: all data can be accessed
//
//└── [Decorator::DataDeclarationNode::RoadDeclarer]
//    Blackboard name: Decorator::DataDeclarationNode::RoadDeclarer
//    - Key: [road], type: [St10shared_ptrIN3abt28DataDeclarationNodeFixture9RoadDummyEE]
//  └── [Composite::Parallel::Unnamed]
//        Blackboard name: Composite::Parallel::Unnamed
//      ├── [Action::TestNodeAccessRoad]
//      │     Blackboard name: Action::TestNodeAccessRoad
//      └── [Decorator::DataDeclarationNode::EgoParamDeclarer]
//            Blackboard name: Decorator::DataDeclarationNode::EgoParamDeclarer
//            - Key: [ego_target_speed], type: [St10shared_ptrIdE]
//            - Key: [ego_id], type: [St10shared_ptrIjE]
//          └── [Action::TestNodeAccessAllParams]
//                Blackboard name: Action::TestNodeAccessAllParams
TEST_F(DataDeclarationNodeFixture, symbol_declaration_node_legal_setup) {
  // Root node: makes road downstream available
  DataDeclaration::UPtr road_declarer = std::make_unique<RoadInserter>();
  DecoratorNode::Ptr road_declarer_decorator =
      std::make_shared<DataDeclarationNode>("RoadDeclarer", std::move(road_declarer));

  // A parallel node to fork the tree into two subtrees (is the child of road_declarer_decorator)
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  road_declarer_decorator->setChild(parallel);

  // Child 1: needs only access to road
  parallel->addChild(std::make_shared<TestNodeAccessRoad>());

  // Child 2: Declares further variables for subtree
  DataDeclaration::UPtr ego_param = std::make_unique<EgoParamInserter>();
  DecoratorNode::Ptr ego_param_decorator =
      std::make_shared<DataDeclarationNode>("EgoParamDeclarer", std::move(ego_param));
  ego_param_decorator->setChild(std::make_shared<TestNodeAccessAllParams>());
  parallel->addChild(std::move(ego_param_decorator));

  // The the collision will be observed in three ticks, which causes a failure downstream:
  road_declarer_decorator->distributeData();
  // Distribute multiple times to check if the result is still consistent.
  road_declarer_decorator->distributeData();
  road_declarer_decorator->distributeData();
  EXPECT_EQ(road_declarer_decorator->executeTick(), NodeStatus::kSuccess);
}

// Illegal tree: not accessible data requested
//
//└── [Decorator::DataDeclarationNode::RoadDeclarer]
//    Blackboard name: Decorator::DataDeclarationNode::RoadDeclarer
//    - Key: [road], type: [St10shared_ptrIN3abt28DataDeclarationNodeFixture9RoadDummyEE]
//  └── [Composite::Parallel::Unnamed]
//        Blackboard name: Composite::Parallel::Unnamed
//      ├── [Action::TestNodeAccessAllParams]
//      │     Blackboard name: Action::TestNodeAccessAllParams
//      └── [Decorator::DataDeclarationNode::EgoParamDeclarer]
//            Blackboard name: Decorator::DataDeclarationNode::EgoParamDeclarer
//          └── [Action::TestNodeAccessAllParams]
//                Blackboard name: Action::TestNodeAccessAllParams
TEST_F(DataDeclarationNodeFixture, symbol_declaration_node_illegal_setup) {
  // Root node: makes road downstream available
  DataDeclaration::UPtr road_declarer = std::make_unique<RoadInserter>();
  DecoratorNode::Ptr road_declarer_decorator =
      std::make_shared<DataDeclarationNode>("RoadDeclarer", std::move(road_declarer));

  // Parallel node to fork the tree into two subtrees (is the child of road_declarer_decorator)
  CompositeNode::Ptr parallel = std::make_shared<ParallelNode>();
  road_declarer_decorator->setChild(parallel);

  // Child 1: needs only access to road
  parallel->addChild(std::make_shared<TestNodeAccessAllParams>());

  // Child 2: Declares further variables for subtree
  DataDeclaration::UPtr ego_param = std::make_unique<EgoParamInserter>();
  DecoratorNode::Ptr ego_param_decorator =
      std::make_shared<DataDeclarationNode>("EgoParamDeclarer", std::move(ego_param));
  ego_param_decorator->setChild(std::make_shared<TestNodeAccessAllParams>());
  parallel->addChild(std::move(ego_param_decorator));

  // The the collision will be observed in three ticks, which causes a failure downstream:
  EXPECT_ANY_THROW(road_declarer_decorator->distributeData(););
}

}  // namespace yase
