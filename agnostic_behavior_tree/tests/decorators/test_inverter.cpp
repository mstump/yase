/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/actions/analyse_nodes.h"
#include "agnostic_behavior_tree/decorator/inverter_node.h"

#include <gtest/gtest.h>

namespace yase {

// Test if child is initialised and terminated
TEST(InverterTest, test_onInit_and_onTerminate) {
  std::shared_ptr<AnalyseNode> dummy_node = std::make_shared<AnalyseNode>(1);

  DecoratorNode::Ptr test_decorator = std::make_shared<InverterNode>();
  test_decorator->setChild(dummy_node);

  // Init and tick of parallel (should init children as well)
  test_decorator->onInit();
  EXPECT_EQ(dummy_node->isInitialised(), true);
  test_decorator->onTerminate();
  EXPECT_EQ(dummy_node->isInitialised(), false);
}

TEST(InverterTest, inverter_node) {
  InverterNode inverter_1;
  inverter_1.setChild(std::make_shared<AlwaysFailure>());
  EXPECT_EQ(inverter_1.executeTick(), NodeStatus::kSuccess);

  InverterNode inverter_2;
  inverter_2.setChild(std::make_shared<AlwaysFailure>());
  EXPECT_EQ(inverter_2.executeTick(), NodeStatus::kSuccess);

  InverterNode inverter_3;
  inverter_3.setChild(std::make_shared<AlwaysFailure>());
  EXPECT_EQ(inverter_3.executeTick(), NodeStatus::kSuccess);
}

}  // namespace yase
