/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/inverter_node.h"

#include <exception>

namespace yase {

InverterNode::InverterNode(Extension::UPtr extension_ptr) : DecoratorNode("Inverter", std::move(extension_ptr)) {}

NodeStatus InverterNode::tick() {
  const NodeStatus child_state = child().executeTick();

  switch (child_state) {
    case NodeStatus::kSuccess: {
      return NodeStatus::kFailure;
    }
    case NodeStatus::kFailure: {
      return NodeStatus::kSuccess;
    }
    case NodeStatus::kRunning: {
      return NodeStatus::kRunning;
    }
    default: {
      std::string error_msg = "The child node [";
      error_msg.append(child().name());
      error_msg.append("] returned unknown NodeStatus.");
      throw std::invalid_argument(error_msg);
    }
  }
}

}  // namespace yase