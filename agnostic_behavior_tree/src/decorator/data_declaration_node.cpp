/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/data_declaration_node.h"

#include <vector>

namespace yase {

yase::DataDeclarationNode::DataDeclarationNode(const std::string& name,
                                               std::vector<DataDeclaration::UPtr> declarations,
                                               yase::Extension::UPtr extension_ptr)
    : DecoratorNode(std::string("DataDeclaration[").append(name).append("]"), std::move(extension_ptr)),
      m_managed_declarations(std::move(declarations)) {}

yase::DataDeclarationNode::DataDeclarationNode(const std::string& name,
                                               yase::DataDeclaration::UPtr declaration,
                                               yase::Extension::UPtr extension_ptr)
    : DataDeclarationNode(name, instantiateInContainer(std::move(declaration)), std::move(extension_ptr)) {}

yase::NodeStatus yase::DataDeclarationNode::tick() {
  if (hasChild()) {
    return child().executeTick();
  }
  return NodeStatus::kSuccess;
}

void yase::DataDeclarationNode::lookupAndRegisterData(yase::Blackboard& blackboard) {
  for (auto& declaration : m_managed_declarations) {
    declaration->lookupAndRegisterData(blackboard);
  }
}

std::vector<DataDeclaration::UPtr> yase::DataDeclarationNode::instantiateInContainer(
    yase::DataDeclaration::UPtr managed_declaration) {
  std::vector<DataDeclaration::UPtr> vector;
  vector.push_back(std::move(managed_declaration));
  return vector;
}

}  // namespace yase