/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/start_at_node.h"

namespace yase {

StartAtNode::StartAtNode(Condition::UPtr condition, Extension::UPtr extension_ptr)
    : DecoratorNode(std::string("StartAt[").append(condition->name).append("]"), std::move(extension_ptr)),
      m_condition(std::move(condition)) {}

NodeStatus StartAtNode::tick() {
  if (!m_done) {
    if (!m_condition->evaluate()) {
      return NodeStatus::kRunning;
    } else {
      m_done = true;
      child().onInit();
    }
  }
  return child().executeTick();
}

void StartAtNode::onInit() {
  m_done = false;
  m_condition->onInit();
}

void StartAtNode::lookupAndRegisterData(Blackboard& blackboard) { m_condition->lookupAndRegisterData(blackboard); }

}  // namespace yase