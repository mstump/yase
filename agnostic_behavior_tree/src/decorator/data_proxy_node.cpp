/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/decorator/data_proxy_node.h"

#include <vector>

namespace yase {

Blackboard::ProxyTable yase::ProxySetting::createProxyTableAndDeclareData(yase::Blackboard&) {
  return Blackboard::ProxyTable();
}

DataProxyNode::DataProxyNode(const std::string& name, ProxySetting::UPtr proxy_setting, Extension::UPtr extension_ptr)
    : DecoratorNode(std::string("DataProxy[").append(name).append("]"), std::move(extension_ptr)),
      m_proxy_setting(std::move(proxy_setting)) {}

NodeStatus DataProxyNode::tick() {
  if (hasChild()) {
    return child().executeTick();
  }
  return NodeStatus::kSuccess;
}

void DataProxyNode::lookupAndRegisterData(Blackboard& blackboard) {
  Blackboard::ProxyTable remapped_keys = m_proxy_setting->createProxyTableAndDeclareData(blackboard);
  m_blackboard->setProxyTable(remapped_keys);
}

}  // namespace yase
