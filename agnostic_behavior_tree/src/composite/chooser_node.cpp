/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/composite/chooser_node.h"

namespace yase {

ChooserNode::ChooserNode(const std::string& name) : ChooserNode(name, nullptr) {}

ChooserNode::ChooserNode(Extension::UPtr extension_ptr) : ChooserNode("Unnamed", std::move(extension_ptr)) {}

ChooserNode::ChooserNode(const std::string& name, Extension::UPtr extension_ptr)
    : CompositeNode(std::string("Selector[").append(name).append("]"), std::move(extension_ptr)) {}

void ChooserNode::onInit() { m_selected_child_index.reset(); }

std::unique_ptr<size_t> ChooserNode::getSelectedChildIndex() const {
  if (m_selected_child_index == nullptr) {
    return nullptr;
  }
  return std::make_unique<size_t>(*m_selected_child_index);
}

NodeStatus ChooserNode::tick() {
  if (childrenCount() == 0) {
    // No children -> success
    return NodeStatus::kSuccess;
  }

  // First call: iterate over sequence and choose index (first execute != kFailure)
  if (m_selected_child_index == nullptr) {
    for (size_t index = 0; index < childrenCount(); ++index) {
      BehaviorNode& child_node = child(index);
      child_node.onInit();
      const NodeStatus child_status = child_node.executeTick();

      switch (child_status) {
        case NodeStatus::kRunning:
          m_selected_child_index = std::make_unique<size_t>(index);
          return NodeStatus::kRunning;
        case NodeStatus::kFailure:
          // Continue with next child
          child_node.onTerminate();
          break;
        case NodeStatus::kSuccess:
          // We are done. Terminate and return success
          m_selected_child_index = std::make_unique<size_t>(index);
          child_node.onTerminate();
          return NodeStatus::kSuccess;
        default: {
          std::string error_msg = "The child node [";
          error_msg.append(child_node.name());
          error_msg.append("] returned unknown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }
  } else {
    const auto last_status = status();

    if (last_status == NodeStatus::kRunning) {
      // Index already selected. Execute again
      BehaviorNode& child_node = child(*m_selected_child_index);
      const NodeStatus child_status = child_node.executeTick();
      if (child_status == NodeStatus::kFailure || child_status == NodeStatus::kSuccess) {
        child_node.onTerminate();
      }
      return child_status;
    }
    return last_status;
  }

  // No more child -> chooser failed
  return NodeStatus::kFailure;
}

}  // namespace yase
