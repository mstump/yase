/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/composite/parallel_node.h"

#include <exception>

namespace yase {

std::string ParallelNode::toStr(const ParallelNode::StopPolicy& stop_policy) {
  switch (stop_policy) {
    case StopPolicy::kAllChildrenSucceed:
      return "AllChildrenSucceed";
    case StopPolicy::kFirstChildSucceeds:
      return "FirstChildSucceeds";
    case StopPolicy::kAnyChildSucceeds:
      return "AnyChildSucceeds";
  }
  throw std::invalid_argument("Unknown ParallelNode::StopPolicy - Cannot convert it to string!");
}

ParallelNode::ParallelNode(const std::string& name, const ParallelNode::Config config)
    : ParallelNode(name, nullptr, config) {}

ParallelNode::ParallelNode(Extension::UPtr extension_ptr, const ParallelNode::Config config)
    : ParallelNode("Unnamed", std::move(extension_ptr), config) {}

ParallelNode::ParallelNode(const std::string& name, Extension::UPtr extension_ptr, const ParallelNode::Config config)
    : CompositeNode(
          std::string("ParallelUntil[").append(toStr(config.stop_policy)).append("][").append(name).append("]"),
          std::move(extension_ptr)),
      m_config{config} {}

void ParallelNode::onInit() {
  m_finished_children.clear();
  for (auto& child : m_children_nodes) {
    child->onInit();
  }
}

NodeStatus ParallelNode::tick() {
  NodeStatus return_status = NodeStatus::kSuccess;

  // Loop over all children
  for (size_t index = 0; index < childrenCount(); index++) {
    if (m_finished_children.find(index) == m_finished_children.end()) {
      BehaviorNode& child = this->child(index);
      const NodeStatus child_status = child.executeTick();

      switch (child_status) {
        case NodeStatus::kRunning: {
          if (return_status != NodeStatus::kFailure &&
              (m_config.stop_policy == StopPolicy::kAllChildrenSucceed ||
               (m_config.stop_policy == StopPolicy::kFirstChildSucceeds && index == 0) ||
               (m_config.stop_policy == StopPolicy::kAnyChildSucceeds && m_finished_children.empty()))) {
            return_status = NodeStatus::kRunning;
          }
          break;
        }
        case NodeStatus::kFailure: {
          m_finished_children.insert(index);
          return_status = NodeStatus::kFailure;
          break;
        }
        case NodeStatus::kSuccess: {
          if (return_status != NodeStatus::kFailure && (m_config.stop_policy == StopPolicy::kAnyChildSucceeds)) {
            return_status = NodeStatus::kSuccess;
          }

          child.onTerminate();
          m_finished_children.insert(index);
          break;
        }
        default: {
          std::string error_msg = "The child node [";
          error_msg.append(child.name());
          error_msg.append("] returned unknown NodeStatus.");
          throw std::invalid_argument(error_msg);
        }
      }
    }
  }

  return return_status;
}

}  // namespace yase