/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "agnostic_behavior_tree/utils/manipulators.h"

namespace yase {

BehaviorManipulator::BehaviorManipulator(BehaviorNode& behavior) : m_behavior(behavior) {}

bool BehaviorManipulator::hasParent() const { return m_behavior.hasParent(); }

void BehaviorManipulator::setParent(BehaviorNode& node) { m_behavior.setParent(node); }

void BehaviorManipulator::setParent(BehaviorNode::Ptr node) { m_behavior.setParent(*node); }

void BehaviorManipulator::clearParent() const {
  m_behavior.m_parent_node = nullptr;
  m_behavior.m_blackboard->clear();
}

BehaviorManipulator manipulator(BehaviorNode& behavior) { return BehaviorManipulator(behavior); }

BehaviorManipulator manipulator(BehaviorNode::Ptr behavior) { return BehaviorManipulator(*behavior); }

DecoratorManipulator manipulator(DecoratorNode& decorator) { return DecoratorManipulator(decorator); }

DecoratorManipulator manipulator(DecoratorNode::Ptr decorator) { return DecoratorManipulator(*decorator); }

CompositeManipulator manipulator(CompositeNode& composite) { return CompositeManipulator(composite); }

CompositeManipulator manipulator(CompositeNode::Ptr composite) { return CompositeManipulator(*composite); }

DecoratorManipulator::DecoratorManipulator(DecoratorNode& decorator)
    : BehaviorManipulator(decorator), m_decorator(decorator) {}

void DecoratorManipulator::setChild(BehaviorNode::Ptr child) { m_decorator.setChild(child); }

bool DecoratorManipulator::hasChild() const { return m_decorator.hasChild(); }

BehaviorNode::Ptr DecoratorManipulator::detachChild() {
  // Return empty child if non is set
  if (!m_decorator.m_child_node) {
    return m_decorator.m_child_node;
  }
  // Detach from parent
  manipulator(m_decorator.m_child_node).clearParent();
  auto tmp = m_decorator.m_child_node;
  m_decorator.m_child_node.reset();
  return tmp;
}

CompositeManipulator::CompositeManipulator(CompositeNode& composite)
    : BehaviorManipulator(composite), m_composite(composite) {}

void CompositeManipulator::addChild(BehaviorNode::Ptr child) { m_composite.addChild(child); }

bool CompositeManipulator::hasChild(size_t index) const { return index < m_composite.m_children_nodes.size(); }

void CompositeManipulator::insertChild(BehaviorNode::Ptr child, size_t index) {
  m_composite.addCheck(child);
  if (index >= m_composite.childrenCount()) {
    std::string error_msg = "Trying to insert child to CompositeNode [";
    error_msg.append(m_composite.name());
    error_msg.append("]: at index [");
    error_msg.append(std::to_string(index));
    error_msg.append("] but it is out of bounds [");
    error_msg.append(std::to_string(m_composite.childrenCount()));
    error_msg.append("].");
    throw std::invalid_argument(error_msg);
  }
  manipulator(child).setParent(m_composite);
  m_composite.m_children_nodes.insert(
      m_composite.m_children_nodes.begin() + static_cast<CompositeNode::NodeVector::difference_type>(index), child);
}

BehaviorNode::Ptr CompositeManipulator::replaceChild(BehaviorNode::Ptr child, size_t index) {
  m_composite.addCheck(child);
  if (index >= m_composite.childrenCount()) {
    std::string error_msg = "Trying to replace child of CompositeNode [";
    error_msg.append(m_composite.name());
    error_msg.append("] with index [");
    error_msg.append(std::to_string(index));
    error_msg.append("] but it has only [");
    error_msg.append(std::to_string(m_composite.childrenCount()));
    error_msg.append("] child behaviors.");
    throw std::invalid_argument(error_msg);
  }
  // Get old object
  BehaviorNode::Ptr ret = m_composite.m_children_nodes[index];
  // Overwrite with new object
  manipulator(child).setParent(m_composite);
  m_composite.m_children_nodes[index] = child;
  // Detach old object and return
  manipulator(ret).clearParent();
  return ret;
}

BehaviorNode::Ptr CompositeManipulator::detachChild(size_t index) {
  m_composite.checkedIndex(index);
  auto iter = m_composite.m_children_nodes.begin() + static_cast<CompositeNode::NodeVector::difference_type>(index);
  BehaviorNode::Ptr ret = *iter;
  m_composite.m_children_nodes.erase(iter);
  manipulator(ret).clearParent();
  return ret;
}

}  // namespace yase