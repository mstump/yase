/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/tree_print.h"

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/utils/visitors.h"

namespace yase {

std::string nodeToStr(const BehaviorNode& node, const bool ansi_escape_code, const bool print_ticks) {
  std::string ret;
  if (node.tickedInCurrentCycle()) {
    ret = toStr(node.status(), ansi_escape_code);
  } else {
    ret = toStr(node.status(), false);
  }
  if (print_ticks) {
    ret.append(" - ").append(std::to_string(node.tickCycle()));
  }
  ret.append(std::string(" - [").append(node.name()).append("]"));
  if (!node.executionInfo().empty()) {
    ret.append(" - ").append(node.executionInfo());
  }
  return ret;
}

void printTree(const BehaviorNode& node, std::ostream& out_stream) {
  std::function<void(const BehaviorNode&, const std::string& start_prefix, const std::string& follow_prefix)>
      print_name_function;
  print_name_function = [&out_stream](const BehaviorNode& node_to_print, const std::string& start_prefix,
                                      const std::string&) {
    out_stream << start_prefix << "[" << node_to_print.name() << "]" << std::endl;
  };
  // Print the tree starting from node
  out_stream << "-------- Behavior Tree --------" << std::endl;
  applyPrintVisitor(node, print_name_function);
}

void printTreeWithRegisteredBlackboardData(const BehaviorNode& node, std::ostream& out_stream) {
  std::function<void(const BehaviorNode&, const std::string& start_prefix, const std::string& follow_prefix)>
      print_node_with_symbol_function;

  // Prints the node with symbols and remapping
  print_node_with_symbol_function = [&out_stream](const BehaviorNode& node_to_print, const std::string& start_prefix,
                                                  const std::string& follow_prefix) {
    out_stream << start_prefix << "[" << node_to_print.name() << "]" << std::endl;
    // print local symbols
    node_to_print.blackboardBase()->printLocalSymbols(follow_prefix + std::string("  "));
  };
  // Print the tree starting from node
  out_stream << "-------- Behavior Tree with registered blackboard data --------" << std::endl;
  applyPrintVisitor(node, print_node_with_symbol_function);
}

void printTreeWithStates(const BehaviorNode& node, std::ostream& out_stream) {
  std::function<void(const BehaviorNode&, const std::string& start_prefix, const std::string& follow_prefix)>
      print_node_with_state_function;

  // Prints the node with state and execution info
  print_node_with_state_function = [&out_stream](const BehaviorNode& node_to_print, const std::string& start_prefix,
                                                 const std::string& /*follow_prefix*/) {
    out_stream << start_prefix << nodeToStr(node_to_print, true, true) << std::endl;
  };
  // Print the tree starting from node
  out_stream << "-------- Behavior Tree with execution states and tick counter --------" << std::endl;
  applyPrintVisitor(node, print_node_with_state_function);
}

void printTreeWithFailureTraces(const BehaviorNode& node, std::ostream& out_stream) {
  std::function<void(const BehaviorNode&, const std::string& start_prefix, const std::string& follow_prefix)>
      print_node_and_trace_failure;

  // Prints the node with Failure highlighting
  print_node_and_trace_failure = [&out_stream](const BehaviorNode& node_to_print, const std::string& start_prefix,
                                               const std::string& /*follow_prefix*/) {
    if (node_to_print.status() == NodeStatus::kFailure) {
      out_stream << start_prefix << nodeToStr(node_to_print, true) << std::endl;
    } else {
      out_stream << start_prefix << nodeToStr(node_to_print, false) << std::endl;
    }
  };
  // Print the tree starting from node
  out_stream << "-------- Behavior Tree with failure trace --------" << std::endl;
  applyPrintVisitor(node, print_node_and_trace_failure);
}

}  // namespace yase
