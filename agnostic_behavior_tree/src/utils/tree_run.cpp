/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/tree_run.h"

#include "agnostic_behavior_tree/utils/tree_print.h"

namespace yase {

void runTree(yase::BehaviorNode::Ptr behavior_to_simulate, const bool print_each_state) {
  runTree(*behavior_to_simulate, print_each_state);
}

void runTree(yase::BehaviorNode& sim_behavior, const bool print_each_state) {
  // Prepare: Distribute data and init tree
  sim_behavior.distributeData();
  sim_behavior.onInit();

  // Simulate: Run simulation until Success/Failure
  yase::NodeStatus status = yase::NodeStatus::kRunning;
  while (status == yase::NodeStatus::kRunning) {
    status = sim_behavior.executeTick();
    if (print_each_state) {
      yase::printTreeWithStates(sim_behavior);
    }
  }

  // Cleanup: Terminate the behavior tree
  sim_behavior.onTerminate();

  if (status != yase::NodeStatus::kSuccess) {
    std::cout << "Simulation did not end as expected:" << std::endl;
    yase::printTreeWithStates(sim_behavior);
  }
}

}  // namespace yase
