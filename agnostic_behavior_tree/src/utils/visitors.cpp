/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/utils/visitors.h"

#include "agnostic_behavior_tree/behavior_node.h"
#include "agnostic_behavior_tree/composite_node.h"
#include "agnostic_behavior_tree/decorator_node.h"

#include <functional>
#include <memory>

namespace yase {

void applyVisitor(const BehaviorNode& root_node, const std::function<void(const BehaviorNode&)>& visitor) {
  // Apply visitor on this node
  visitor(root_node);
  // Apply on children if any
  if (auto composite = dynamic_cast<const CompositeNode*>(&root_node); composite != nullptr) {
    for (size_t index = 0; index < composite->childrenCount(); ++index) {
      applyVisitor(composite->child(index), visitor);
    }
  } else if (auto decorator = dynamic_cast<const DecoratorNode*>(&root_node); decorator != nullptr) {
    if (decorator->hasChild()) {
      applyVisitor(decorator->child(), visitor);
    }
  }
}

void applyPrintVisitor(
    const BehaviorNode& root_node,
    const std::function<void(const BehaviorNode&, const std::string& start_prefix, const std::string& follow_prefix)>&
        print_visitor) {
  std::function<void(const std::string&, const BehaviorNode&, const bool)> printFunctor;
  printFunctor = [&printFunctor, &print_visitor](const std::string& prefix, const BehaviorNode& node,
                                                 const bool is_this_tail) {
    // Local prefix for this node
    std::string local_prefix;
    // Prefix for deeper nodes
    std::string next_prefix;
    if (is_this_tail) {
      local_prefix = prefix + "└─ ";
      next_prefix = prefix + "   ";
    } else {
      local_prefix = prefix + "├─ ";
      next_prefix = prefix + "│  ";
    }

    // Print local node
    print_visitor(node, local_prefix, next_prefix);

    // Print the children of a composite / decorator
    if (auto composite = dynamic_cast<const CompositeNode*>(&node); composite != nullptr) {
      for (size_t i = 0; i < composite->childrenCount(); i++) {
        bool is_next_tail = false;
        if (i == composite->childrenCount() - 1) {
          is_next_tail = true;
        }
        printFunctor(next_prefix, composite->child(i), is_next_tail);
      }
    } else if (auto decorator = dynamic_cast<const DecoratorNode*>(&node)) {
      if (decorator->hasChild()) {
        printFunctor(next_prefix, decorator->child(), true);
      }
    }
  };

  // Print the tree starting from root_node
  printFunctor("", root_node, true);
}

}  // namespace yase
