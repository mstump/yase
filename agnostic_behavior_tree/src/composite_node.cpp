/*******************************************************************************
 * Copyright (c) Robert Bosch GmbH - 2021-2022
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/composite_node.h"

#include <functional>
#include <algorithm>

namespace yase {

CompositeNode::CompositeNode(const std::string& name) : CompositeNode(name, nullptr) {}

CompositeNode::CompositeNode(const std::string& name, Extension::UPtr extension_ptr)
    : BehaviorNode(std::string("Composite::").append(name), std::move(extension_ptr)) {}

void CompositeNode::addChild(BehaviorNode::Ptr child) {
  addCheck(child);
  child->setParent(*this);
  m_children_nodes.push_back(std::move(child));
}

size_t CompositeNode::childrenCount() const { return m_children_nodes.size(); }

const BehaviorNode& CompositeNode::child(size_t index) const { return checkedIndex(index); }

BehaviorNode& CompositeNode::child(size_t index) { return checkedIndex(index); }

void CompositeNode::distributeData() {
  m_blackboard->clearLocal();
  lookupAndRegisterData(*(m_blackboard));
  std::for_each(m_children_nodes.begin(), m_children_nodes.end(),
                [](BehaviorNode::Ptr& child) { child->distributeData(); });
}

void CompositeNode::onTerminate() {
  std::for_each(m_children_nodes.begin(), m_children_nodes.end(),
                [](BehaviorNode::Ptr& child) { child->onTerminate(); });
}

BehaviorNode& CompositeNode::checkedIndex(size_t index) const {
  if (index >= childrenCount()) {
    std::string error_msg = "Trying to access child of CompositeNode [";
    error_msg.append(name());
    error_msg.append("] with index [");
    error_msg.append(std::to_string(index));
    error_msg.append("] but it has only [");
    error_msg.append(std::to_string(childrenCount()));
    error_msg.append("] child behaviors.");
    throw std::runtime_error(error_msg);
  }
  return *m_children_nodes[index];
}

void CompositeNode::addCheck(const BehaviorNode::Ptr& child) const {
  if (!child) {
    std::string error_msg = "CompositeNode [";
    error_msg.append(name());
    error_msg.append("]: Checked child behavior is NULL.");
    throw std::invalid_argument(error_msg);
  }
  if (child->hasParent()) {
    std::string error_msg = "CompositeNode [";
    error_msg.append(name());
    error_msg.append("]: Checked child behavior is already attached to parent.");
    throw std::invalid_argument(error_msg);
  }
}

}  // namespace yase