/*******************************************************************************
 * Copyright (c) Max Paul Bauer - Robert Bosch GmbH - 2021
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "agnostic_behavior_tree/behavior_node.h"

#include <exception>
#include <string>

namespace yase {

std::string toStr(NodeStatus status, bool use_ansi_escape_code) {
  if (!use_ansi_escape_code) {
    switch (status) {
      case NodeStatus::kSuccess:
        return "SUCCESS";
      case NodeStatus::kFailure:
        return "FAILURE";
      case NodeStatus::kRunning:
        return "RUNNING";
      case NodeStatus::kIdle:
        return "IDLE";
    }
  } else {
    switch (status) {
      case NodeStatus::kSuccess:
        return "\x1b[32m"
               "SUCCESS"
               "\x1b[0m";
      case NodeStatus::kFailure:
        return "\x1b[31m"
               "FAILURE"
               "\x1b[0m";
      case NodeStatus::kRunning:
        return "\x1b[36m"
               "RUNNING"
               "\x1b[0m";
      case NodeStatus::kIdle:
        return "IDLE";
    }
  }
  return "Undefined status!";
}

NodeStatus BehaviorNode::executeTick() {
  // Track current tick cycle
  if (m_parent_node != nullptr) {
    if (m_tick_cycle >= m_parent_node->m_tick_cycle) {
      std::string error_msg = "Error while executing tick() of behavior node [";
      error_msg.append(name());
      error_msg.append("]: Tick cycle [")
          .append(std::to_string(m_tick_cycle))
          .append("] of this node is greater/equal then its parent tick cycle [");
      error_msg.append(std::to_string(m_parent_node->m_tick_cycle))
          .append("] - A child is not allowed to proceed faster then its parent");
      throw std::logic_error(error_msg);
    }
    m_tick_cycle = m_parent_node->m_tick_cycle;
  } else {
    (m_tick_cycle)++;
  }
  // Clear execution info before step
  m_execution_info.clear();
  m_status = tick();
  if (m_status == NodeStatus::kIdle) {
    std::string error_msg = "Error while executing tick() of behavior node [";
    error_msg.append(name());
    error_msg.append("]: Returned invalid status NodeStatus::kIdle!");
    throw std::logic_error(error_msg);
  }
  return status();
}

void BehaviorNode::distributeData() {
  m_blackboard->clearLocal();
  lookupAndRegisterData(*(m_blackboard));
}

void BehaviorNode::onTerminate() {}

NodeStatus BehaviorNode::status() const { return m_status; }

std::string BehaviorNode::executionInfo() const { return m_execution_info; }

const std::string& BehaviorNode::name() const { return m_name; }

Blackboard::Ptr BehaviorNode::blackboardBase() const { return m_blackboard; }

unsigned int BehaviorNode::tickCycle() const { return m_tick_cycle; }

bool BehaviorNode::tickedInCurrentCycle() const {
  if (m_parent_node == nullptr) {
    return true;
  }
  if (m_parent_node->tickedInCurrentCycle()) {
    return (m_parent_node->tickCycle() == m_tick_cycle);
  }
  return false;
}

const Extension& BehaviorNode::extension() const { return extensionChecked(); }

bool BehaviorNode::hasParent() const { return m_parent_node != nullptr; }

void BehaviorNode::lookupAndRegisterData(Blackboard&) {}

void BehaviorNode::executionInfo(const std::string& new_execution_info) { m_execution_info = new_execution_info; }

BehaviorNode::BehaviorNode(const std::string& name, Extension::UPtr extension_ptr)
    : m_name(name), m_blackboard(std::make_shared<BlackboardOwner>(name)), m_extension(std::move(extension_ptr)) {}

Extension& BehaviorNode::extensionChecked() const {
  if (!extensionExists()) {
    std::string exception_msg = "Trying to access the extension in node [";
    exception_msg.append(name());
    exception_msg.append("] which is NULL.");
    throw std::runtime_error(exception_msg);
  }
  return *m_extension;
}

void BehaviorNode::setParent(BehaviorNode& node) {
  m_parent_node = &node;
  m_blackboard->setParentBlackboard(node.m_blackboard);
}

}  // namespace yase
