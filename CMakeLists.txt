# ##############################################################################
# Copyright (c) 2022 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ##############################################################################

cmake_minimum_required(VERSION 3.16.3)

if(NOT DEFINED Yase_IS_TOP_LEVEL)
  set(Yase_IS_TOP_LEVEL OFF)
  if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    set(Yase_IS_TOP_LEVEL ON)
  endif()
endif()

project(
  Yase
  VERSION 0.9.0
  DESCRIPTION "A simulator agnostic scenario/simulation engine"
  HOMEPAGE_URL https://gitlab.eclipse.org/eclipse/simopenpass/yase
  LANGUAGES CXX
)

option(Yase_BUILD_TESTS "Build the tests" ${Yase_IS_TOP_LEVEL})
option(Yase_BUILD_DOCS "Build the documentation (requires Doxygen and Graphviz)" ${Yase_IS_TOP_LEVEL})
option(Yase_INSTALL "Enable installation (projects embedding Yase may want to turn this OFF)" ${Yase_IS_TOP_LEVEL})

include(CMakeDependentOption)
cmake_dependent_option(Yase_USE_SYSTEM_GTEST "Use system GTest" OFF "Yase_BUILD_TESTS" OFF)

if(Yase_INSTALL)
  include(GNUInstallDirs)
  set(Yase_CONFIG_INSTALL_DIR
      ${CMAKE_INSTALL_DATADIR}/Yase/cmake
      CACHE PATH "Config install directory"
  )
  set(Yase_INCLUDE_INSTALL_DIR
      ${CMAKE_INSTALL_INCLUDEDIR}
      CACHE PATH "Include install directory"
  )
endif()

if(Yase_BUILD_TESTS)
  enable_testing()
  if(Yase_USE_SYSTEM_GTEST)
    find_package(GTest REQUIRED)
    if(NOT TARGET GTest::gtest_main AND TARGET GTest::Main)
      # CMake <3.20 FindGTest module provides the deprecated target GTest::Main while CMake >=3.20 uses GTest's own
      # package config which provides the target GTest::gtest_main
      add_library(GTest::gtest_main ALIAS GTest::Main)
    endif()
  else()
    include(FetchContent)
    FetchContent_Declare(
      googletest
      GIT_REPOSITORY https://github.com/google/googletest.git
      GIT_TAG v1.13.0
    )
    set(INSTALL_GTEST OFF)
    FetchContent_MakeAvailable(googletest)
  endif()
endif()

add_subdirectory(agnostic_behavior_tree)

export(
  TARGETS agnostic_behavior_tree
  NAMESPACE Yase::
  FILE ${CMAKE_CURRENT_BINARY_DIR}/YaseTargets.cmake
)

if(Yase_INSTALL)
  install(TARGETS agnostic_behavior_tree EXPORT YaseTargets)

  install(
    EXPORT YaseTargets
    NAMESPACE Yase::
    DESTINATION ${Yase_CONFIG_INSTALL_DIR}
  )

  include(CMakePackageConfigHelpers)
  configure_package_config_file(
    cmake/YaseConfig.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/YaseConfig.cmake
    INSTALL_DESTINATION ${Yase_CONFIG_INSTALL_DIR}
    NO_SET_AND_CHECK_MACRO
  )

  write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/YaseConfigVersion.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY SameMajorVersion
  )

  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/YaseConfig.cmake ${CMAKE_CURRENT_BINARY_DIR}/YaseConfigVersion.cmake LICENSE
          DESTINATION ${Yase_CONFIG_INSTALL_DIR}
  )
endif()
